import { app } from 'hyperapp'

import view from './view'
import state from './modules/state'
import actions from './modules/actions'

// types
import { Actions, State } from './modules/types'

app<State, Actions>(
  state,
  actions,
  view,
  document.getElementById('app')
)
