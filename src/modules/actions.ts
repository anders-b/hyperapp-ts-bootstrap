import { ActionsType } from 'hyperapp'
import { State, Actions } from './types'

const actions: ActionsType<State, Actions> = {
  change: count => state => ({ ...state, count })
}

export default actions
