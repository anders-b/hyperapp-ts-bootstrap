export interface State {
  count: number
}

export interface Actions {
  change(value: number): State
}
